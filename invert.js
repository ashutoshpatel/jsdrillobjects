// create invert function
function invert(obj) {

    // if the object don't have any keys means the object is empty
    if(Object.keys(obj).length == 0){
        // so return null
        return null;
    }

    // create empty object to store result
    let answer = {};

    // use for-in loop to iterate on object
    for(let key in obj){
        // add value as a key and key as a value in answer object
        answer[obj[key]] = key;
    }

    // return answer object
    return answer;
}

// export the invert function
module.exports = invert;
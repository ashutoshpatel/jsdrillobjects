// create defaults function
function defaults(obj, defaultProps) {

    // if the object don't have any keys means the object is empty
    if(Object.keys(obj).length == 0){
        // so return null
        return null;
    }
    
    // use for-in loop to iterate on object
    for(let key in obj){
        // check the current key is defaultProps or not
        if(defaultProps.hasOwnProperty(key)){
            // if yes then update the key value with defaultProps value
            obj[key] = defaultProps[key];
        }
    }

    // return the object
    return obj;
}

// export the defaults function
module.exports = defaults;
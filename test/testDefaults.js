// import the defaults function
const defaults = require('../defaults.js');

// const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// create defaultName
const defaultName = {name : undefined};

// call the defaults function
const result = defaults(testObject, defaultName);

if(result !== null){
    // print the result
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}
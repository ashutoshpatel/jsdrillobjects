// import invert function
const invert = require('../invert.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// call the invert function
const result = invert(testObject);

if(result !== null){
    // print the result
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}
// import the keys function
const keys = require('../keys.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// call the keys function
const result = keys(testObject);

if(result !== null){
    // print the result
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}
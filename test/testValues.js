// import values function
const values = require('../values.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// call the values function
const result = values(testObject);

if(result !== null){
    // print the result
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}
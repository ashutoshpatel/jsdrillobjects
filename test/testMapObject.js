// import the mapObject function
const mapObject = require('../mapObject.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// call the mapObject function
const result = mapObject(testObject , function(values, key) {
    return values + 5;
});

if(result !== null){
    // print the result
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}
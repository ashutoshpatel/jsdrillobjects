// import pairs function
const pairs = require('../pairs.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// call the pairs function
const result = pairs(testObject);

if(result !== null){
    // print the result
    console.log(result);
}
else{
    // means the object is empty
    console.log("Object is empty");
}
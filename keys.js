// create the keys function
function keys(obj) {
    
    // if the object don't have any keys means the object is empty
    if(Object.keys(obj).length == 0){
        // so return null
        return null;
    }
    
    // create empty array to store keys of object
    const arr = [];

    // use for-in loop to iterate on object
    for(let keys in obj){
        // add keys in arr array
        arr.push(keys);
    }

    // return the array
    return arr;
}

// export the keys function
module.exports = keys;
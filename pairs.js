// create pairs function
function pairs(obj) {

    // if the object don't have any keys means the object is empty
    if(Object.keys(obj).length == 0){
        // so return null
        return null;
    }

    // create empty array to store key & value pairs
    const arr = [];

    // use for-in loop to iterate on object
    for(let keys in obj){
        // add key & value pairs with new array in arr array 
        arr.push([keys,obj[keys]]);
    }

    // return the arr array
    return arr;
}

// export the pairs function
module.exports = pairs;
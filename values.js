// create values function
function values(obj) {

    // if the object don't have any keys means the object is empty
    if(Object.keys(obj).length == 0){
        // so return null
        return null;
    }

    // create empty array to store values of object
    const arr = [];

    // use for-in loop to iterate on object
    for(let keys in obj){
        // add values in arr array
        arr.push(obj[keys]);
    }

    // return the array
    return arr;
}

// export the values function
module.exports = values;
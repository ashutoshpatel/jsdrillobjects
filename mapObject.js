// create mapObject function
function mapObject(obj, cb) {

    // if the object don't have any keys means the object is empty
    if(Object.keys(obj).length == 0){
        // so return null
        return null;
    }

    // use for-in loop to iterate on object
    for(let keys in obj){
        // update object values using callback function
        obj[keys] = cb(obj[keys], keys);
    }

    // return modifiy object
    return obj;
}

// export the mapObject function
module.exports = mapObject;